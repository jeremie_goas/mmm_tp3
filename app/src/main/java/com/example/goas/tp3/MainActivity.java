package com.example.goas.tp3;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements ListViewFragment.OnItemSelectedListener, WebViewFragment.OnButtonLocateClick {

    private boolean smartphone = true;
    private Fragment webViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        smartphone = findViewById(R.id.layout_smartphone) == null? false: true;

        if ( smartphone ){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if ( fragmentTransaction != null ){
                Fragment fragment = new ListViewFragment();
                ((ListViewFragment) fragment).setListener(this);
                fragmentTransaction.add(R.id.framelayout, fragment);
                fragmentTransaction.addToBackStack(null);
            }

            fragmentTransaction.commit();
        }else{

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if ( fragmentTransaction != null ){
                Fragment listFragment = new ListViewFragment();
                ((ListViewFragment) listFragment).setListener(this);
                fragmentTransaction.add(R.id.listFrameLayout, listFragment);

                webViewFragment = new WebViewFragment();
                ((WebViewFragment) webViewFragment).setListener(this);
                fragmentTransaction.add(R.id.webViewFrameLayout, webViewFragment);

            }

            fragmentTransaction.commit();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if ( smartphone ) {
            FragmentManager fragmentManager = getFragmentManager();

            if (fragmentManager.getBackStackEntryCount() > 1)
                fragmentManager.popBackStack();
        }else{
            if ( webViewFragment != null ){
                ((WebViewFragment) webViewFragment).backHistory();
            }
        }
    }


    @Override
    public void onItemSelected(String name, String url, LatLng position) {
        if ( smartphone ){

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if ( fragmentTransaction != null ) {
                Bundle args = new Bundle();
                args.putString("URL", url);
                args.putParcelable("POS", position);
                args.putString("NAME", name);
                Fragment fragment = new WebViewFragment();
                ((WebViewFragment) fragment).setListener(this);
                fragment.setArguments(args);
                fragmentTransaction.replace(R.id.framelayout, fragment);
                fragmentTransaction.addToBackStack(null);
            }

            fragmentTransaction.commit();
        }else{
            if ( webViewFragment != null ){
                ((WebViewFragment) webViewFragment).loadUrl(url);
            }
        }
    }

    @Override
    public void OnButtonLocateClick(final String name,final LatLng position) {
        if ( smartphone ){
            Intent intent = new Intent(this, MapActivity.class);
            intent.putExtra("POS", position);
            intent.putExtra("NAME", name);
            startActivity(intent);
        }else{

            MapFragment fragment = MapFragment.newInstance();

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            Intent intent = getIntent();

            if ( fragmentTransaction != null ){
                fragmentTransaction.replace(R.id.webViewFrameLayout, fragment);
                fragmentTransaction.addToBackStack(null);
            }

            fragmentTransaction.commit();

            fragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    googleMap.addMarker(new MarkerOptions()//Ajout d'un marker sur la carte
                            .position(position)
                            .title(name));

                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, 10);//Center Camera
                            googleMap.animateCamera(cameraUpdate);
                        }
                    });
                }
            });

        }
    }
}
