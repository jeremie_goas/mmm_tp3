package com.example.goas.tp3;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by goas on 15/10/15.
 */
public class ListViewFragment extends Fragment {

    private static final String TAG = ListViewFragment.class.getSimpleName();

    private ListView list;

    private OnItemSelectedListener listener;

    private static Map<String,Map<String,Object>> data = new HashMap<String, Map<String,Object>>();

    static {

        Map<String, Object> properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/alsace/index.shtml" );
        properties.put( "POS", new LatLng( 48.2454402, 6.4161316));
        data.put("Alsace", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/beaujolais/index.shtml" );
        properties.put( "POS", new LatLng( 46.083347, 4.6579122));
        data.put("Beaujolais", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/jura/index.html" );
        properties.put( "POS", new LatLng( 46.7828921, 5.1688018));
        data.put("Jura", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/champagne/index.html" );
        properties.put( "POS", new LatLng( 49.253642, 3.9850484));
        data.put("Champagne", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/savoie/index.html" );
        properties.put( "POS", new LatLng( 45.4947055, 5.8432796));
        data.put("Savoie", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/languedoc/index.html" );
        properties.put( "POS", new LatLng( 43.636424, 1.0234522));
        data.put("Languedoc-Roussillon", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/bordelais/index.html" );
        properties.put( "POS", new LatLng( 44.8638282, -0.6561813));
        data.put("Bordelais", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/cotes_du_rhone/index.html" );
        properties.put( "POS", new LatLng( 44.6835657, 3.7349573));
        data.put("Vallée du Rhone", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/provence/index.html" );
        properties.put( "POS", new LatLng( 46.097262, 4.5722252));
        data.put("Provence", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/val_de_loire/index.html");
        properties.put( "POS", new LatLng( 47.9135477, 1.7594827));
        data.put("Val de Loire", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/sud-ouest/index.html" );
        properties.put( "POS", new LatLng( 44.8802308, -1.5943457));
        data.put("Sud-Ouest", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/corse/index.html" );
        properties.put( "POS", new LatLng( 42.1771653, 7.9287458));
        data.put("Corse", properties );

        properties = new HashMap<String,Object>();
        properties.put( "URL", "http://technoresto.org/vdf/bourgogne/index.html" );
        properties.put( "POS", new LatLng( 47.2744622, 3.0612084));
        data.put("Bourgogne", properties );
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.fragment_listview, container, false);

        ArrayAdapter adapter = new ArrayAdapter(view.getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, getKeys(data) );
        list = (ListView) view.findViewById(R.id.listview);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = ((TextView) view.findViewById(android.R.id.text1)).getText().toString();

                Map<String, Object> properties = data.get(text);

                if ( properties == null )
                    Log.e( TAG, "Error on item");
                else{
                    String url = (String) properties.get("URL");
                    LatLng pos = (LatLng) properties.get("POS");

                    if ( url == null || url.isEmpty() || pos == null ){
                        Log.e( TAG, "Error on url or on position");
                    }else{
                        if ( listener != null )
                            listener.onItemSelected(text, url, pos);
                    }
                }
            }
        });

        return view;
    }

    public void setListener( OnItemSelectedListener listener ){
        this.listener = listener;
    }

    public interface OnItemSelectedListener {
        public void onItemSelected(String name, String url, LatLng position);
    }

    public List<String> getKeys( Map<String, ? extends Object> map ){
        List<String> keys = new ArrayList<String>();
        for( String key : map.keySet() ){
            keys.add(key);
        }
        return keys;
    }

}
