package com.example.goas.tp3;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by goas on 15/10/15.
 */
public class WebViewFragment extends Fragment {

    private static final String TAG = WebViewFragment.class.getSimpleName();

    private TextView information;

    private WebView webView;

    private String url;

    private String name;

    private Button locate;

    private LatLng position;

    private OnButtonLocateClick listener;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.fragment_web_view, container, false);

        Bundle arguments = getArguments();

        webView = (WebView) view.findViewById(R.id.webView);
        information = (TextView) view.findViewById(R.id.information);
        locate = (Button) view.findViewById(R.id.btn_locate);

        String urlParam = null;

        if ( arguments != null && arguments.containsKey("NAME") && arguments.containsKey("URL") && arguments.containsKey("POS")){
            urlParam = arguments.getString("URL");
            name = arguments.getString("NAME");
            position = (LatLng) arguments.getParcelable("POS");
        }

        loadUrl( urlParam );

        bindButton();

        return view;
    }

    public void bindButton(){
        locate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( listener != null ){
                    listener.OnButtonLocateClick(name, position);
                }
            }
        });
    }

    public void setListener( OnButtonLocateClick listener ){
        this.listener = listener;
    }


    public interface OnButtonLocateClick {
        public void OnButtonLocateClick(String name, LatLng position);
    }

    public void loadUrl( String url ){
        if ( url != null && !url.isEmpty() ){
            if ( !url.equals(this.url) ) {
                webView.loadUrl(url);
                this.url = url;
            }
            information.setVisibility(View.GONE);
            webView.setVisibility( View.VISIBLE );
        }else{
            information.setVisibility( View.VISIBLE );
            webView.setVisibility( View.GONE );
        }
    }

    /**
     * Back history
     */
    public void backHistory(){
        this.webView.goBack();
    }
}
